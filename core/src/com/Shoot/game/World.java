/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Shoot.game;

/**
 *
 * @author dutui1804
 */
public class World {
    public static final float GRAVITY = -20;
    
    private Block[][] blocks;
    private Commando player;
    
    public World()
    {
        createDemoWorld();
    }
    
    public void createDemoWorld()
    {
        player = new Commando(1,2); // create bob
        
        blocks = new Block[20][8]; // create a 20x8 map
        
        // populating the map with blocks
        for(int i = 0; i < 20; i++)
        {
            blocks[i][0] = new Block(i,0); // bottom row
            blocks[i][6] = new Block(i,6); // top rows
            blocks[i][7] = new Block(i,7); // see above
            if(i <=6)// almost bottom row
            {
                blocks[i][1] = new Block(i,1); 
            }
            if(i<= 5 && i >=2) // left hand wall
            {
                blocks[0][i] = new Block(0,i);
            }
            if(i<= 5 && i >=3) // hanging down part
            {
                blocks[3][i] = new Block(3,i);
                blocks[4][i] = new Block(4,i);
            }
        }
    }
    
    public int getWidth()
    {
        return blocks.length;
    }

    public Commando getPlayer() {
        return player;
    }
    
    public Block getBlock(int row, int col)
    {
        return blocks[row][col];
    }
    
}
