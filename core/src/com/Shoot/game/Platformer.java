/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Shoot.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

/**
 *
 * @author dutui1804
 */
public class Platformer extends ApplicationAdapter {
    
        private World world;
        private WorldRenderer renderer;
        private WorldController control;
        
        	@Override
	public void create () {
            world = new World();
            renderer = new WorldRenderer(world);
            control = new WorldController(world);
            
            // sets the controller to process any commands
            Gdx.input.setInputProcessor(control);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
                control.update(Gdx.graphics.getDeltaTime());
                renderer.render(); // show the world
	}
        
        @Override
        public void resize(int width, int height)
        {
            renderer.setSize(width, height);
        }
}
