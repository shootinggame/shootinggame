/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Shoot.game;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
  // Create a bullet class
  // Handle gun in the commando (player class)
/**
 *
 * @author dutui1804
 */
public class Commando {
    
    //public static final float SIZE = 0.5f; 
    public static final float SIZE = 0.5f; //
    public static final float SPEED = 2f; //
    public static final float JUMP_VEL = 1f;

    private static final float ACCELERATION = 20;
    private static final float MAX_JUMP_SPEED = 7;
    private static final float DAMP = 0.9f;
    private static final float MAX_VELOCITY = 4;
    
    
    public enum State
    {
        IDLE, WALKING, JUMPING, DYING, SHOOTING, RUNNING
    }
    
    private Rectangle bounds;
    private Vector2 position, velocity, acceleration;
    private boolean facingLeft;
    private State state;
    private float stateTime = 0f;
    
    
    public Commando(float x, float y)
    {
        position = new Vector2(x,y);
        bounds = new Rectangle(x,y, SIZE, SIZE);
        velocity = new Vector2(0,0);
        acceleration = new Vector2(0,0);
        facingLeft = false;
        state = State.IDLE;
    }
        
    public float getX()
    {
        return position.x;
    }
    
    public float getY()
    {
        return position.y;
    }
    
    public void setX(float x)
    {
        position.x = x;
    }
    
    public void setY(float y)
    {
        position.y = y;
    }
    
    public State getState()
    {
        return state;
    }
    
    public boolean facingLeft()
    {
        return facingLeft;
    }
    
    public void setFacingLeft(boolean b)
    {
        facingLeft = b;
    }
    
    public void setXVel(float v)
    {
        velocity.x = v;
    }
    
    public void setYVel(float v)
    {
        velocity.y = v;
    }
    
    public float getXVel()
    {
        return velocity.x;
    }
    
    public float getYVel()
    {
        return velocity.y;
    }
    
    public void setXAccel(float a)
    {
        acceleration.x = a;
    }
    
    public void setYAccel(float a)
    {
        acceleration.y = a;
    }
    
    public float getXAccel()
    {
        return acceleration.x;
    }
    
    public float getYAccel()
    {
        return acceleration.y;
    }
    
    public void setState(State s)
    {
        state = s;
    }
    
    public void jump()
    {
        velocity.y = MAX_JUMP_SPEED;
    }
    
    public void update(float delta) 
    {
        stateTime += delta;
        
        acceleration.y = World.GRAVITY;
        acceleration.scl(delta);
        velocity.add(acceleration);
        if(acceleration.x == 0)
        {
            velocity.x *= DAMP; // slowing bob down
        }
        // can't run any faster then allowed
        if(velocity.x > MAX_VELOCITY)
        {
            velocity.x = MAX_VELOCITY;
        }else if(velocity.x < -MAX_VELOCITY)
        {
            velocity.x = -MAX_VELOCITY;
        }

        // adds the velocity to player's position based on time
        position.add(velocity.x*delta, velocity.y*delta);
        
     }
    
    public float getStateTime()
    {
        return stateTime;
    }
    
}
