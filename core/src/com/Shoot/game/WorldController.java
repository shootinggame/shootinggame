/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Shoot.game;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;

/**
 *
 * @author dutui1804
 */
public class WorldController implements InputProcessor{
    
    private static final long LONG_JUMP_PRESS = 150;
        
    private boolean right=false, left=false, jump=false, shoot=false;
    
    private World world;
    private Commando player;
    private long jumpPressTime;
    
    public WorldController(World world)
        {
        this.world = world;
        player = world.getPlayer();
        }
    
    private void processInput()
    {
        // left key being pressed
        if(left)
        {
            player.setFacingLeft(true);
            player.setXVel(-Commando.SPEED);
            if(player.getState() != Commando.State.JUMPING)
            {
                player.setState(Commando.State.WALKING);
            }
        }
        if(right)
        {
            player.setFacingLeft(false);
            player.setXVel(Commando.SPEED);
            if(player.getState() != Commando.State.JUMPING)
            {
                player.setState(Commando.State.WALKING);
            }
        }
        // if neither right or left being pressed
        // OF both right and left are pressed
        if((!right && !left) || (right && left))
        {
            player.setXVel(0);
            player.setXAccel(0);
            if(player.getState() != Commando.State.JUMPING)
            {
                player.setState(Commando.State.IDLE);
            }
        }
        
        // jumping
        if(jump)
        {
            // if commando is not jumping
            if(player.getState() != Commando.State.JUMPING)
            {
                jumpPressTime = System.currentTimeMillis();
                player.setState(Commando.State.JUMPING);
                player.jump();
            }else
            {
                
                // check to see if we have held the jump for maximum time
                if((System.currentTimeMillis() - jumpPressTime) > LONG_JUMP_PRESS)
                {
                    jump = false;
                }else
                {
                    player.jump();
                }
            }
        }
    }
    
    public void checkCollisions()
    {
        int x = (int)player.getX();
        int y = (int)player.getY();
        int endX = (int)(player.getX() + Commando.SIZE);
        int endY = (int)(player.getY() + Commando.SIZE);
        
        
        // handling collisions
        // based on the Seperate Axis Theorm (SAT)
        
        for(int i = x; i <= endX; i++)
        {
            for(int j = y; j <= endY; j++)
            {
                // collision
                if(world.getBlock(i, j) != null)
                {
                    // hit something so calculate overlap
                    float overX = 0;
                    float overY = 0;
                    
                    if(x < i) // collision on right
                    {
                        overX = player.getX() + Commando.SIZE - i;
                    }else // left collision
                    {
                        overX = i + Block.SIZE - player.getX();
                    }
                    
                    if(y < j) // collision on the top
                    {
                        overY = player.getY() + Commando.SIZE - j;
                    }else // collision on the bottom
                    {
                        overY = j + Block.SIZE - player.getY();
                    }
                    
                    // deal with collision
                    // the axis with the least amount of overlap
                    if(overY < overX)
                    {
                        player.setYVel(0);
                        if(y < j) // top
                        {
                            player.setY(player.getY() - overY);
                        }else // hit bottom
                        {
                            if(player.getState() == Commando.State.JUMPING)
                            {
                                player.setState(Commando.State.IDLE);
                                jump = false;
                            }
                            player.setY(player.getY() + overY);
                        }
                    }else
                    {
                        player.setXVel(0);
                        if(x < i) // right side
                        {
                            player.setX(player.getX() - overX);
                        }else
                        {
                            player.setX(player.getX() + overX);
                        }
                    }
                    
                }
            }
        }
    }
    
    public void update(float delta)
    {
        processInput();
              
        player.update(delta);

        // falling through the screen
        if(player.getY() < 0) 
        {
            player.setY(0);
            if(player.getState() == Commando.State.JUMPING)
            {
                player.setState(Commando.State.IDLE);
            }
        }
        
        // running off back of screen
        if(player.getX() < 0)
        {
            player.setX(0);
            if(player.getState() == Commando.State.JUMPING)
            {
                player.setState(Commando.State.IDLE);
            }
        }
        
        checkCollisions();
    }
    
    @Override
    public boolean keyDown(int keycode) {
        if(keycode == Input.Keys.LEFT)
        {
            left = true;
        }
        if(keycode == Input.Keys.RIGHT)
        {
            right = true;
        }
        if(keycode == Input.Keys.SPACE)
        {
            jump = true;
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        if(keycode == Input.Keys.LEFT)
        {
            left = false;
        }
        if(keycode == Input.Keys.RIGHT)
        {
            right = false;
        }
        if(keycode == Input.Keys.SPACE)
        {
            jump = false;
        }
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
    
    
}
