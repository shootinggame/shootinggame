/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Shoot.game;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 *
 * @author dutui1804
 */
public class WorldRenderer {
    private TextureAtlas atlas = new TextureAtlas("images/images.pack");
    private TextureRegion blockImage;
    private TextureRegion bulletImage;
    private TextureRegion commandoImage;
    private Animation walkLeft;
    private Animation walkRight;
    private Animation runLeft;
    private Animation runRight;
    private TextureRegion idleLeft;
    private TextureRegion idleRight;
    
    private World theWorld;
    private Commando player;
    
    private OrthographicCamera camera;
    private SpriteBatch batch;
    
    private float width;
    private float height;
    private float ppuX;
    private float ppuY;

 public WorldRenderer(World world)
    {
        theWorld = world;
        player = world.getPlayer();
        
        blockImage = atlas.findRegion("block");
        idleLeft = atlas.findRegion("commando", 1);
        idleRight = new TextureRegion(idleLeft);
        idleRight.flip(true, false);
        
        TextureRegion[] frames = new TextureRegion[5];
        for(int i = 0; i < 5; i++)
        {
            frames[i] = atlas.findRegion("commando", i+2);
        }
        // copy the array so i can flip images safely
        runLeft = new Animation(1/15f,frames.clone());
        
        for(int i = 0; i < 5; i++)
        {
            frames[i] = new TextureRegion(frames[i]);
            frames[i].flip(true,false); // face right
        }
        runRight = new Animation(1/15f, frames);
        
        batch = new SpriteBatch();
        camera = new OrthographicCamera(10,8);
        camera.position.set(5f,4f,0);
        camera.update();
    }
    
    public void setSize(float width, float height)
    {
        this.width = width;
        this.height = height;
        ppuX = this.width/camera.viewportWidth;
        ppuY = this.height/camera.viewportHeight;
    }
    
    public void render()
    {
        // check camera based on bob
        float camX = camera.position.x;
        if(player.getX() < camera.viewportWidth/2)
        {
            camera.position.x = camera.viewportWidth/2;
        }else  if(player.getX() > theWorld.getWidth() - camera.viewportWidth/2)
        {
            camera.position.x = theWorld.getWidth() - camera.viewportWidth/2;
        }else
        {
            camera.position.x = player.getX();
        }     
        camera.update();
        
            camera.position.y = player.getY();
            
            
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        
        // start drawing the blocks
        int startX = (int)(Math.floor(camera.position.x - camera.viewportWidth/2));
        int endX = (int)(Math.ceil(camera.position.x + camera.viewportWidth/2));
        for(int i=startX; i < endX; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                if(theWorld.getBlock(i,j) != null)
                {
                    // draw(texture, x, y, width, height) 
                    // scales the image to the proper size
                    batch.draw(blockImage, i, j, Block.SIZE, Block.SIZE);
                }
            }
        } // end drawing blocks
        if(player.getState() == Commando.State.IDLE)
        {
            if(player.facingLeft())
            {
                batch.draw(idleLeft, player.getX(), player.getY(), Commando.SIZE, Commando.SIZE);
            }else
            {
                batch.draw(idleRight, player.getX(), player.getY(), Commando.SIZE, Commando.SIZE);
            }
        }else if(player.getState() == Commando.State.WALKING)
        {
            if(player.facingLeft())
            {
                // get the keyframe from the animation
                // this is based on the state time passed
                batch.draw(runLeft.getKeyFrame(player.getStateTime(),true), player.getX(), player.getY(), Commando.SIZE, Commando.SIZE);
            }else
            {
                batch.draw(runRight.getKeyFrame(player.getStateTime(),true), player.getX(), player.getY(), Commando.SIZE, Commando.SIZE);
            }
        }else if(player.getState() == Commando.State.JUMPING)
        {
            if(player.facingLeft())
            {
                batch.draw(idleLeft, player.getX(), player.getY(), Commando.SIZE, Commando.SIZE);
            }else
            {
                batch.draw(idleRight, player.getX(), player.getY(), Commando.SIZE, Commando.SIZE);
            }
        }
        
        batch.end();
    }
    
}

