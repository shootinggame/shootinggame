/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Shoot.game;

import java.awt.Rectangle;

/**
 *
 * @author dutui1804
 */
public class Bullet {
    
    private int speed;
    private int damage;
    private int x,y;
    
    public Bullet(int x , int y, int damage)
    {
      this.x = x;
      this.y = y;
      this.damage = damage;
 
      this.speed = 5;
    }
    
    public Rectangle getRect()
    {
       return new Rectangle(x, y,5,5);
    }
    
    
    public double getX()
    {
    return this.x;
    }
    
    public double getY()
    {
    return this.y;
    }   
    
    public int getDamage()
    {
    return this.damage;                                                                                                                     
    }                                                                           
    
    public int getSpeed()
    {
    return this.speed;
    }
    
    public void move()
    {
    this.y -=speed;    
    }
    
}
